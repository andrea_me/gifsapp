import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { Gif, SearchGifsResponse } from './../interface/gifs.interface';

@Injectable({
  providedIn: 'root',
})
export class GifsService {
  private apiKey: string = 'Z3622uyaR5nLOlVWHG3ncGkyDo1nByrx';
  private servicioUrl: string = 'https://api.giphy.com/v1/gifs';

  //_ --> es por convención de que la propiedad es privada
  private _historial: string[] = [];

  get historial() {
    // clonamos el arreglo de historial con el operador spread,
    // para que este disponible para los otros componentes y no se modifique el original
    return [...this._historial];
  }

  public resultados: Gif[] = [];

  constructor(private http: HttpClient) {
    /**
     * Creates an instance of GifsService, solo se ejecuta una única vez, cuando es llamado.
     *
     * se guarda en historial, las busquedas anteriores que se obtienen con localStorage
     * debido a que getItem puede regresar un null, se coloca ! indicando que siempre tendrá un valor
     * o de lo contrario, regrese un []
     */
    this._historial = JSON.parse(localStorage.getItem('historial')!) || [];
    this.resultados = JSON.parse(localStorage.getItem('historialResultados')! ) || [];
  }

  buscarGifs(query: string) {
    //limpiamos el termino a ingresar al historial, para que no tenga espacios y no haya conflicto
    query = query.trim().toLocaleLowerCase();

    //si el termino a ingresar no esta ya declarado en el arreglo
    if (!this._historial.includes(query)) {
      //ingresa el nuevo termino al historial
      this._historial.unshift(query);

      //corta el historial a 10 elementos
      this._historial = this._historial.slice(0, 10);

      /*localStorage -- es propio de JS,
        setItem() -- establece un item con key: value
        JSON -- un objeto que proporciona funciones para convertir valores a y desde el formato JSON
        stringify -- convierte un objeto a uns cadena, conservando {"" : ,}
        */
      localStorage.setItem('historial', JSON.stringify(this._historial));
    }

    const params = new HttpParams()
                    .set('api_key', this.apiKey)
                    .set('limit','10')
                    .set('q',query);

    this.http.get<SearchGifsResponse>(`${ this.servicioUrl }/search`, {params})
      .subscribe((res) => {
        //subscribe, se ejecuta cuando se tenga una solución al http.get
        this.resultados = res.data;
        localStorage.setItem('historialResultados', JSON.stringify(this.resultados));
      });
  }
}
