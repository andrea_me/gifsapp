import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

import { GifsService } from './../services/gifs.service';

@Component({
  selector: 'app-busqueda',
  templateUrl: './busqueda.component.html',
  styles: [],
})
export class BusquedaComponent implements OnInit {
  /**
   * @ViewChild es un decorador de propiedad.
   * En este caso, nos permite obtener referencias a objectos html
   * es como un querySelector que obtiene el id="txtBuscar" para poder manipularlo
   *
   * txtBuscar!, el ! --> indica un non null assertion operator
   *                      que evita que TS informe que una propiedad es nula o indefinida
   *                      Se ocupa en casos en donde se esta segurto que dicha propiedad
   *                      siempre tendrá un valor.
   *
   * AL indicarle que el selector es una referencia local(id#),
   * devuelve un tipo ElementRef<HTMLInputElemen>,
   * este tipo generico se debe a que se trata de un input,
   * al colocarlo el Intellisence ya detecta los metodos y eventos propios de input
   */
  @ViewChild('txtBuscar') txtBuscar!: ElementRef<HTMLInputElement>;

  constructor(private gifsService:GifsService) {}

  ngOnInit(): void {}

  buscar() {
    //almacenamos el valor que se pasa en el input
    const valor = this.txtBuscar.nativeElement.value;

    //valida que el termino ingresado no sea un string vacío
    if(valor.trim().length === 0){
      return;
    }

    //lo mandamos al servicio de gifs para que ese valor se almacene
    this.gifsService.buscarGifs(valor);

    //Con esa referencia, podemos limpiar el input después de realizar una búsqueda
    this.txtBuscar.nativeElement.value = '';
  }
}
